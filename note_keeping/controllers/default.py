def index():
	redirect(URL('note_viewing'))

def note_viewing():
	if request.vars.Submition=="Insert":
		redirect(URL('note_making'))
	if request.vars.DeletionSubmit=="DeleteAll":
		db.notes.truncate()
	db(db.notes.id == request.vars.delete_id).delete()
	return dict()

def note_making():
	if request.vars.SubmitSecond=="InputIn":
		db.notes.insert(title = request.vars.title, content = request.vars.content);
		redirect(URL('note_viewing'))
	return dict()

def note_editing():
	if request.vars.SubmitSecond=="InputIn":
		db.notes.insert(title = request.vars.title, content = request.vars.content);
		redirect(URL('note_viewing'))
	row = db(db.notes.id == request.vars.view_id).select()
	for i in row:
		session.title = str(i.title)
		session.content = str(i.content)
	db(db.notes.id == request.vars.view_id).delete()
	return dict()