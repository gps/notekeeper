# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
from gluon.tools import Mail
from ast import literal_eval

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Welcome to web2py!")
    return dict(message=T('Hello World'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())

def counter():
    if session.counter >= 0:
        session.counter = session.counter + 1
    else:
        session.counter = 0
    return dict(counter=session.counter)


def home():
    return dict()

@auth.requires_login()
def note_viewing(): 
    l = auth.user
    l = l["first_name"]
    if request.vars.Submition=="Insert":
        redirect(URL('note_making'))
    elif request.vars.DeletionSubmit=="DeleteAll":
        db.notes.truncate()
    elif request.vars.Search=="Search":
        redirect(URL('note_searching'))
    db(db.notes.id == request.vars.delete_id).delete()
    return dict(l=l)

def get_tasks():
    tasks = db(db.ToDoList.username==auth.user.first_name).select()
    json='{ "todo" : [' 
    count = 0
    for i in tasks:
        json = json + '"' + i["task"] + '"'
        count = count + 1
        if count!=3:
            json = json + ','
        else:
            break
    json = json + ']}'
    print json
    return json

@auth.requires_login()
def note_making():
    l = auth.user.first_name
    if request.vars.SubmitSecond=="InputIn":
        print request.vars
        request.vars.content = request.vars.content.split('\r\n')
        taglist = request.vars.tags.split(',')
        request.vars.tags = ', '
        for i in taglist:
            if (i!='') and i.isspace()==False:
                i = i.strip()
                if db.tags(db.tags.tag==i)==None:
                    db.tags.insert(tag = i)
                request.vars.tags = request.vars.tags + i + ', '
        db.notes.insert(n_title = request.vars.title,n_tags = request.vars.tags, n_content = str(request.vars.content),n_user=auth.user.id,n_color=request.vars.color, n_modified = str(request.now), textfield=request.vars.Text, canvas_image=request.vars.canvas_image, canvas_height=request.vars.canvas_height);
        redirect(URL('note_viewing'))
    elif request.vars.SubmitSecond=="Remind Me":
        request.vars.content = request.vars.content.split('\r\n')
        taglist = request.vars.tags.split(',')
        request.vars.tags = ', '
        for i in taglist:
            if (i!='') and i.isspace()==False:
                i = i.strip()
                if db.tags(db.tags.tag==i)==None:
                    db.tags.insert(tag = i)
                request.vars.tags = request.vars.tags + i + ', '
        db.notes.insert(n_title = request.vars.title,n_msg=request.vars.n_msg,a1=request.vars.n_date,a2=request.vars.n_time,n_tags = request.vars.tags, n_content = str(request.vars.content),n_user=auth.user.id,n_color=request.vars.color, n_modified = str(request.now),textfield=request.vars.Text, canvas_image=request.vars.canvas_image, canvas_height=request.vars.canvas_height);  
        db.Set_up.insert(Set_up_e=auth.user.email,Set_up_m=request.vars.n_msg,Set_up_d=request.vars.n_date,Set_up_t=request.vars.n_time)     
        redirect(URL('note_viewing'))
    return dict(l = l)

@auth.requires_login()
def note_editing():
    us = auth.user.first_name
    if request.vars.SubmitSecond=="InputIn":
        request.vars.content = request.vars.content.split('\r\n')
        print request.vars
        taglist = request.vars.tags.split(',')
        request.vars.tags = ', '
        for i in taglist:
            if (i!='') and i.isspace()==False:
                i = i.strip()
                if db.tags(db.tags.tag==i)==None:
                    db.tags.insert(tag = i)
                request.vars.tags = request.vars.tags + i + ', '
        db.notes.insert(n_title = request.vars.title,n_tags = request.vars.tags, n_content = str(request.vars.content),n_msg=request.vars.n_msg,a1=request.vars.n_date,a2=request.vars.n_time,n_user=auth.user.id,n_color=request.vars.color, n_modified = str(request.now),textfield=request.vars.Text, canvas_image=request.vars.canvas_image, canvas_height=request.vars.canvas_height);      
        db.Set_up.insert(Set_up_e=auth.user.email,Set_up_m=request.vars.n_msg,Set_up_d=request.vars.n_date,Set_up_t=request.vars.n_time)
        redirect(URL('note_viewing'))
    row = db(db.notes.id == request.vars.view_id).select()
    for i in row:
        session.title = i.n_title
        session.tags = i.n_tags[2:-2]
        session.color = i.n_color
        if i.a1 == None :
            session.flag = 0
        else:
            session.flag = 1
        session.msg = i.n_msg
        session.date = i.a1
        session.time = i.a2
        session.Text=i.textfield
        session.canvas_image=i.canvas_image
        session.canvas_height=i.canvas_height
        l = literal_eval(i.n_content)
        session.content = ''
        for i in l:
            session.content = session.content + i + '\r\n'
    db(db.notes.id == request.vars.view_id).delete()
    print session.canvas_height
    return dict(us=us,canvas_image=XML(session.canvas_image),canvas_height=XML(session.canvas_height))

@auth.requires_login()
def note_searching():
    if request.vars.Back=="Back":
        redirect(URL('note_viewing'))
    return dict()

@auth.requires_login()
def get_notes():
    rows = db(db.notes.id>0).select()
    json='{"notes":['
    num=0
    i=0
    for r in rows:
        if(i!=0):
            json+=","
        json+='{"id":'+str(r['id']) +', "title":"'+r['n_title']+'", "tags":"'+r['n_tags'][2:-2]+'","modified":"'+str(r['n_modified'])[:10]+'","color":"'+str(r['n_color'])[:10]+'", "content":"'+r['n_content']+'"}'
        i=1
        num+=1
    json+='], "number":'+str(num)+"}"
    return dict(rows=json)

@auth.requires_login()
def upload():
    form = SQLFORM(db.feature)
    if form.process().accepted:
        rows = db(db.feature.id > 0).select()
        l = len(rows)
        rows = rows [l -1]
        fio = open("/home/sowmith/web2py/applications/Register/uploads/"+rows['address'],"r")
        l = fio.read()
        redirect('note_making?SubmitSecond=InputIn&title='+request.vars.title+'&tags='+request.vars.tags+'&content='+l)
    return dict(form=form)

@auth.requires_login()
def mailto():
    form = SQLFORM(db.test)
    if form.process().accepted:
        strftime("%d/%m/%y")
    return dict(form = form)

@auth.requires_login()
def check():
    ans = "false"
    if request.vars.common == "check":
        l = strftime("%d:%m:%y")
        l=str(l)
        if str(request.vars.date)==l:
            ans = "true"
            return ans

def truncate():
    db.notes.truncate('RESTART IDENTITY CASCASE')

@auth.requires_login()
def home1():
    l = auth.user
    l = l["first_name"]
    return dict(l=l)

@auth.requires_login()
def alarm():
    date = strftime("%d-%m-%y")
    time = strftime("%H:%M")
    print date
    print time
    rows = db((db.Set_up.Set_up_d==date) &(db.Set_up.Set_up_t==time)).select()
    print rows
    for row in rows:
        mail.send(row['Set_up_e'],'Note_Keeper_Notification',row['Set_up_m'])
        print "Done"
    return dict()

def note_search():
    taglist = request.vars.tags.split(',')
    tagch = []
    for i in taglist:
        if (i!='') and i.isspace()==False:
            i = i.strip()
            i = ', ' + i + ', '
            if i not in tagch:
                tagch = tagch + [i]
    rows = db( db.notes.n_title.contains(request.vars.title) & db.notes.n_tags.contains(tagch, all=True) & db.notes.n_content.contains(request.vars.content)).select()
    json='{"notes":['
    num=0
    i=0
    for r in rows:
        if(i!=0):
            json+=","
        json+='{"id":'+str(r['id']) +', "title":"'+r['n_title']+'", "tags":"'+r['n_tags'][2:-2]+'", "content":"'+r['n_content']+'"}'
        i=1
        num+=1
    json+='], "number":'+str(num)+"}"
    return dict(rows=json)

def set_alarm():
    if request.vars.SubmitSecond=="Remind Me":  
        db.Set_up.insert(Set_up_e=auth.user.email,Set_up_m=request.vars.n_msg,Set_up_d=request.vars.n_date,Set_up_t=request.vars.n_time)   
        print "hello"
        redirect('note_viewing')
    return dict()

def get_tags():
    rows = db(db.tags.id>0).select()
    json='{"tags":['
    i=0
    for r in rows:
        if(i!=0):
            json+=","
        json+= '"' + r['tag'] + '"'
        i=1
    json+="]}"
    return json

def todolist():
    if request.vars.sub=="Add to List":
        val=False
        db.ToDoList.insert(username=auth.user.first_name,checkbox=val,task=request.vars.content,priority=request.now)
    return dict()

def todolistprint():
    s=""
    new=db().select(db.ToDoList.ALL,orderby=~db.ToDoList.priority)
    for i in new:
        if i.checkbox==True:
            s+="<p id=p" + str(i.id)  + " style='text-decoration:line-through' ><input type='checkbox' id=" + str(i.id) + " onchange='save(" + str(i.id) + ");' checked=true /> " + i.task + " <input type='button' onclick='delitem(" + str(i.id) + ")' value=' X ' /></p><br>"
        else:
            s+="<p id=p" + str(i.id)  + " style='text-decoration:none' ><input type='checkbox' id=" + str(i.id) + " onchange='save(" + str(i.id) + ");' /> " + i.task + " <input type='button' onclick='delitem(" + str(i.id) + ")' value=' X ' /></p><br>"
    return s

def updatelist():
    n=request.vars.n
    db(db.ToDoList.id==int(n)).update(checkbox=request.vars.checked,priority=request.now)

def removelist():
    n=request.vars.n
    db(db.ToDoList.id==int(n)).delete()

def Download():
    redirect("/Register/default/download/feature.address.92528bbd333b0299.4f66666c696e652e7461722e677a.gz")