#!/bin/bash/
for i in {1..20}
do
grep "Title" song$i.lyric |  cut -d ":" -f2 > out
cut -d " " --complement -f1 < out > yes
d=$(sed "s/ /_/g" < yes)
mv song$i.lyric $d.lyric
rm out
rm yes
done
