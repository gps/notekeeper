# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
from gluon.tools import Mail
from time import *

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Welcome to web2py!")
    return dict(message=T('Hello World'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())

def counter():
    if session.counter >= 0:
        session.counter = session.counter + 1
    else:
        session.counter = 0
    return dict(counter=session.counter)


def home():
    return dict()

@auth.requires_login()
def note_viewing():
    l = auth.user
    l = l["first_name"]
    if request.vars.Submition=="Insert":
        redirect(URL('note_making'))
    elif request.vars.DeletionSubmit=="DeleteAll":
        db.notes.truncate()
    elif request.vars.Search=="Search":
        redirect(URL('note_searching'))
    db(db.notes.id == request.vars.delete_id).delete()
    return dict(l=l)

@auth.requires_login()
def note_making():
    l = auth.user.id
    print request.vars
    if request.vars.SubmitSecond=="InputIn":
        db.notes.insert(n_title = request.vars.title,n_tags = request.vars.tags, n_content = request.vars.content,n_user=auth.user.id,n_color=request.vars.color, n_modified = str(request.now));
        redirect(URL('note_viewing'))
    
    elif request.vars.SubmitSecond=="Remainder":
        db.notes.insert(n_title = request.vars.title,n_tags = request.vars.tags, n_content = request.vars.content,n_user=auth.user.id,n_color=request.vars.color, n_modified = str(request.now),n_msg=request.vars.msg, n_time = request.vars.time ,n_date = request.vars.date);
        redirect(URL('note_viewing'))
    
    #elif request.vars.SubmitSecond=="Canvas":
    #    db.notes.insert(n_title = request.vars.title,n_tags = request.vars.tags, n_content = request.vars.content,n_user=auth.user.id,n_color=request.vars.color, n_modified = str(request.now) , n_canvas = request.vars.canvas);
    #    redirect(URL('note_viewing'))
    #elif request.vars.SubmitSecond=="Complex":
    #    db.notes.insert(n_title = request.vars.title,n_tags = request.vars.tags, n_content = request.vars.content,n_user=auth.user.id,n_color=request.vars.color, n_modified = str(request.now),n_msg=request.vars.msg, n_time = request.vars.time ,n_date = request.vars.date, n_canvas = request.vars.canvas );
    #    redirect(URL('note_viewing'))
    
    return dict()

@auth.requires_login()
def note_editing():
    if request.vars.SubmitSecond=="InputIn":
        db.notes.insert(n_title = request.vars.title,n_tags = request.vars.tags, n_content = request.vars.content,n_user=auth.user.id);
        redirect(URL('note_viewing'))
    row = db(db.notes.id == request.vars.view_id).select()
    for i in row:
        session.title = str(i.n_title)
        session.tags = str(i.n_tags)
        session.content = str(i.n_content)
    db(db.notes.id == request.vars.view_id).delete()
    return dict()

@auth.requires_login()
def note_searching():
    rows = []
    if request.vars.Back=="Back":
        redirect(URL('note_viewing'))
    elif request.vars.Search=="Search":
        #rows = db(db.notes.title.contains(request.vars.title) & db.notes.n_content.contains(request.vars.content)).select()
        rows = db(  ( db.notes.n_user == auth.user.id ) & (db.notes.n_tags.contains(request.vars.tags) ) ).select()
        session.title = request.vars.title
        session.tags = request.vars.tags
        session.content = request.vars.content
    else:
        session.title = ''
        session.tags = ''
        session.content = ''
    return dict(rows=rows)


@auth.requires_login()
def get_notes():
    rows = db(db.notes.id>0).select()
    json='{"notes":['
    num=0
    i=0
    for r in rows:
        if(i!=0):
            json+=","
        json+='{"id":'+str(r['id'])+', "title":"'+r['n_title']+'", "tags":"'+r['n_tags']+'", "content":"'+r['n_content']+'"}'
        i=1
        num+=1
    json+='], "number":'+str(num)+"}"
    return dict(rows=json)

@auth.requires_login()
def upload():
    form = SQLFORM(db.feature)
    if form.process().accepted:
        rows = db(db.feature.id > 0).select()
        l = len(rows)
        rows = rows [l -1]
        fio = open("/ug/ug2k13/cse/manepalli.sowmith/Desktop/web2py/applications/Register/uploads/"+rows['address'],"r")
        l = fio.read()
        redirect('note_making?SubmitSecond=InputIn&title='+request.vars.title+'&tags='+request.vars.tags+'&content='+l)
    return dict(form=form)

@auth.requires_login()
def mailto():
    form = SQLFORM(db.test)
    if form.process().accepted:
        strftime("%d/%m/%y")
        #print request.vars
        #l = mail.send('manepalli.sowmith@students.iiit.ac.in','Hi ra','Emi chestunnavu?')
    return dict(form = form)

@auth.requires_login()
def check():
    ans = "false"
    if request.vars.common == "check":
        l = strftime("%d:%m:%y")
        l=str(l)
        if str(request.vars.date)==l:
            ans = "true"
            return ans

def truncate():
    db.feature.truncate('RESTART IDENTITY CASCASE')

@auth.requires_login()
def home1():
    l = auth.user
    l = l["first_name"]
    return dict(l=l)

@auth.requires_login()
def alarm():
    date = strftime("%Y-%m-%d")
    time = strftime("%H:%M")
    rows = db((db.Set_up.Set_up_date==date) &(db.Set_up.Set_up_time[:-3]==time)).select()
    for row in rows:
        mail.send(row['Set_up_email'],'Note_Keeper_Notification',row['Set_up_msg'])
    return dict()

@auth.requires_login()
def note_search():
    rows = db(db.notes.n_title.contains(request.vars.title) & \
            db.notes.n_content.contains(request.vars.content)).select()
    json='{"notes":['
    num=0
    i=0
    for r in rows:
        if(i!=0):
            json+=","
        json+='{"id":'+str(r['id']) +', "title":"'+r['title']+'", "tags":"'+r['tags']+'", "content":"'+r['content']+'"}'
        i=1
        num+=1
    json+='], "number":'+str(num)+"}"
    return dict(rows=json)
